export const space = {
  standard: '0.5rem',
  100: '0.05rem',
  200: '0.2rem',
  300: '0.5rem',
  400: '0.8rem',
  500: '1rem',
  600: '1.5rem',
  700: '2rem',
  800: '3rem',
  900: '4rem',
};
