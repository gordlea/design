export const borderWidths = {
  b1: '1px',
  b2: '0.15rem',
  b3: '0.35rem',
  b4: '0.7rem',
  b5: '1.5rem',
};
export const borderStyles = {
  borderSolid: 'solid',
  borderDotted: 'dotted',
  borderDashed: 'dashed',
  borderGroove: 'groove',
};
