import { styled } from '../../stitches/index.ts';

const Button = styled('button', {
  width: 'fit-content',
  paddingBottom: '$200',
  paddingTop: '$200',
  paddingLeft: '$300',
  paddingRight: '$300',
  margin: '$200',

  borderRadius: '$100',
  cursor: 'pointer',
  color: '$textHigh',
  backgroundColor: '$brandSolidBg',
  '&:hover': {
    borderColor: '$textHigh',
    backgroundColor: `$brandHoveredSolidBg`,
  },
  '&:active': {
    backgroundColor: `$textHigh`,
    color: '$brandSolidBg',
  },
  borderWidth: '$b2',
  borderColor: '$brandSolidBg',
  borderStyle: '$borderSolid',
  fontFamily: '$main',
  variants: {
    size: {
      small: {
        fontSize: '$200',
        fontWeight: '$light',
      },
      medium: {
        fontSize: '$400',
        fontWeight: '$xlight',
      },
      large: {
        fontSize: '$600',
        fontWeight: '$thin',
      },
    },
    type: {
      brand: {
        color: '$brand11',
        backgroundColor: '$secondary07',
        borderColor: '$secondary07',
        '&:hover': {
          borderColor: '$brandTextLow',
          backgroundColor: `$secondary08`,
        },
        '&:active': {
          backgroundColor: `$brand11`,
          color: '$secondary07',
        },
      },
      secondary: {
        color: '$textHigh',
        backgroundColor: '$secondarySolidBg',
        borderColor: '$secondarySolidBg',
        '&:hover': {
          borderColor: '$textHigh',
          backgroundColor: `$secondaryHoveredSolidBg`,
          color: '$textHigh',
        },
        '&:active': {
          backgroundColor: `$textHigh`,
          color: `$secondarySolidBg`,
        },
      },
      tertiary: {
        color: '$textMed',
        backgroundColor: '$elementBg',
        borderColor: '$textMed',
        '&:hover': {
          backgroundColor: `$subtleBorder`,
          borderColor: '$textHigh',
          color: '$textHigh',
        },
        '&:active': {
          backgroundColor: '$textMed',
          color: '$elementBg',
        },
      },
    },
  },
});

export default Button;
