import React from 'react';
import Button from '../src';

export default {
  parameters: {
    layout: 'centered',
  },
};

const buttonSizes = ['small', 'medium', 'large'];
const buttonVariants = ['primary', 'secondary'];
// const buttonState = ['enabled', 'disabled'];

export const ButtonBrand: Story<{ visible: boolean }> = ({ visible }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Button type="brand" size="small">
        Small Brand Button
      </Button>

      <Button type="brand" size="medium">
        Medium Brand Button
      </Button>

      <Button type="brand" size="large">
        Large Brand Button
      </Button>
    </div>
  );
};

export const ButtonPrimary: Story<{ visible: boolean }> = ({ visible }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Button type="primary" size="small">
        Small Primary Button
      </Button>

      <Button type="primary" size="medium">
        Medium Primary Button
      </Button>

      <Button type="primary" size="large">
        Large Primary Button
      </Button>
    </div>
  );
};

export const ButtonSecondary: Story<{ visible: boolean }> = ({ visible }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Button type="secondary" size="small">
        Small Secondary Button
      </Button>

      <Button type="secondary" size="medium">
        Medium Secondary Button
      </Button>

      <Button type="secondary" size="large">
        Large Secondary Button
      </Button>
    </div>
  );
};

export const ButtonTertiary: Story<{ visible: boolean }> = ({ visible }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Button type="tertiary" size="small">
        Small Tertiary Button
      </Button>

      <Button type="tertiary" size="medium">
        Medium Tertiary Button
      </Button>

      <Button type="tertiary" size="large">
        Large Tertiary Button
      </Button>
    </div>
  );
};
