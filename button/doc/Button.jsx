import { styled } from '@stitches/react';

const Button = styled('button', {
  width: 'fit-content',
  paddingBottom: '$200',
  paddingTop: '$200',
  paddingLeft: '$300',
  paddingRight: '$300',
  margin: '$200',

  borderRadius: '$100',
  cursor: 'pointer',
  color: '$colorBackgroundtext',
  backgroundColor: '$backgroundBrand',
  '&:hover': {
    backgroundColor: `$backgroundBrandHovered`,
  },
  '&:active': {
    backgroundColor: `$backgroundBrandHovered`,
  },
  border: 0,
  fontFamily: '$main',
  variants: {
    size: {
      small: {
        fontSize: '$200',
      },
      medium: {
        fontSize: '$400',
      },
      large: {
        fontSize: '$600',
      },
    },
    type: {
      primary: {
        color: '$colorBackgroundtext',
        backgroundColor: '$backgroundBrand',
        '&:hover': {
          backgroundColor: `$backgroundBrandHovered`,
        },
        '&:active': {
          backgroundColor: `$backgroundBrandHovered`,
        },
      },
      secondary: {
        color: '$textFade',
        backgroundColor: '$backgroundApp',
        '&:hover': {
          backgroundColor: `$backgroundSurface`,
        },
        '&:active': {
          backgroundColor: `$backgroundSurface`,
        },
        border: 'solid 2px $textFade',
      },
    },
  },
});

export default Button;
