import {
  sand,
  blueDark,
  sandDark,
  orangeDark,
  greenDark,
  redDark,
  yellowDark,
  purpleDark,
  cyanDark,
  bronzeDark,
  blackA,
} from '@radix-ui/colors';
import { defaultThemeMap } from '@stitches/core';

interface colorCfg {
  name: string;
  colorName: string;
}

const c = {
  ...blueDark,

  ...sandDark,
  ...orangeDark,

  ...greenDark,
  ...redDark,
  ...yellowDark,
  ...purpleDark,
  ...cyanDark,
  ...bronzeDark,
  ...blackA,
};

function getFullName(name: string, postFix: string): string {
  if (name.length === 0) {
    return `${postFix.charAt(0).toLowerCase()}${postFix.slice(1)}`;
  }
  return `${name}${postFix}`;
}

function generateNumberedColors(colorCfgs: colorCfg[]) {
  const result = {};
  for (const cfg of colorCfgs) {
    for (let i = 1; i <= 12; i++) {
      result[getFullName(cfg.name, `${i}`.padStart(2, '0'))] =
        c[`${cfg.colorName}${i}`];
    }
  }
  return result;
}

function generateNamedColors(colorCfgs: colorCfg[]) {
  const result = {};
  for (const cfg of colorCfgs) {
    result[getFullName(cfg.name, 'AppBg')] = c[`${cfg.colorName}1`];
    result[getFullName(cfg.name, 'SurfaceBg')] = c[`${cfg.colorName}2`];
    result[getFullName(cfg.name, 'ElementBg')] = c[`${cfg.colorName}3`];

    result[getFullName(cfg.name, 'HoveredElementBg')] = c[`${cfg.colorName}4`];
    result[getFullName(cfg.name, 'ActiveElementBg')] = c[`${cfg.colorName}5`];

    result[getFullName(cfg.name, 'SubtleBorder')] = c[`${cfg.colorName}6`];
    result[getFullName(cfg.name, 'ElementBorder')] = c[`${cfg.colorName}7`];
    result[getFullName(cfg.name, 'HoveredElementBorder')] =
      c[`${cfg.colorName}8`];

    result[getFullName(cfg.name, 'SolidBg')] = c[`${cfg.colorName}9`];
    result[getFullName(cfg.name, 'HoveredSolidBg')] = c[`${cfg.colorName}10`];

    result[getFullName(cfg.name, 'TextLow')] = c[`${cfg.colorName}11`];
    result[getFullName(cfg.name, 'TextHigh')] = c[`${cfg.colorName}12`];
  }
  return result;
}

const colorCfg = [
  {
    name: '',
    colorName: 'sand',
  },
  {
    name: 'brand',
    colorName: 'red',
  },
  {
    name: 'secondary',
    colorName: 'blue',
  },
  {
    name: 'tertiary',
    colorName: 'blue',
  },
  { name: 'success', colorName: 'green' },
  { name: 'warning', colorName: 'yellow' },
  { name: 'danger', colorName: 'red' },
  { name: 'emerg', colorName: 'purple' },
  { name: 'alert', colorName: 'purple' },
  { name: 'crit', colorName: 'purple' },
  { name: 'err', colorName: 'red' },
  { name: 'warning', colorName: 'yellow' },
  { name: 'notice', colorName: 'blue' },
  { name: 'info', colorName: 'green' },
  { name: 'debug', colorName: 'sand' },
  { name: 'blackA', colorName: 'blackA' },
];

const colors = {
  ...generateNumberedColors(colorCfg),
  ...generateNamedColors(colorCfg),
};

colors.textMed = sand.sand7;

export { colors };
