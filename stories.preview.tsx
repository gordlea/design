import React from 'react';
import { styled } from './stitches/index.ts';

const Wrapper = styled('div', {
  backgroundColor: '$surfaceBg',
  padding: '$500',
});

export const parameters = {
  layout: 'centered',
};

export const decorators = [
  (Story) => {
    return (
      <Wrapper>
        <Story />
      </Wrapper>
    );
  },
];
