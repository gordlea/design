import React from 'react';
import Accordion from '../src';

export default {
  parameters: {
    layout: 'centered',
  },
};

const buttonSizes = ['small', 'medium', 'large'];
const buttonVariants = ['primary', 'secondary'];
// const buttonState = ['enabled', 'disabled'];

export const AccordionStory: Story<{ visible: boolean }> = ({ visible }) => {
  return <Accordion></Accordion>;
};
