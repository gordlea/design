import { Root as AccordRoot } from '@radix-ui/react-accordion';

const Accordion = styled(AccordRoot, {
  borderRadius: 6,
  width: '100%',
  backgroundColor: mauve.mauve6,
  boxShadow: `0 2px 10px $blackA04`,
});

export default Accordion;
