export const radii = {
  100: '0.4rem',
  200: '0.6rem',
  radiusRound: '1e5px',
};
