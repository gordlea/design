export const fontSizes = {
  100: '0.7rem',
  200: '0.85rem',
  300: '1.0rem',
  400: '1.2rem',
  500: '1.4rem',
  600: '1.7rem',
  700: '2rem',
  800: '2.5rem',
  900: '3rem',
};

export const fonts = {
  main: 'ui-sans-serif, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif',
  mono: '"SF Mono",SFMono-Regular, ui-monospace,"DejaVu Sans Mono", Menlo, Consolas, monospace',
};

export const fontWeights = {
  thin: 100,
  xlight: 200,
  light: 300,
  normal: 400,
  semibold: 500,
  bold: 600,
};

export const lineHeights = {
  condensedUltra: 1,
  condensed: 1.25,
  default: 1.5,
};

export const letterSpacings = {
  tight: '0.01rem',
  loose: '0.2rem',
};
